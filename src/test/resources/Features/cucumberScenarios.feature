Feature: feature to navigate and accept cookies in SGS portal

  Background: 
    Given User navigates to SGS portal

  #TC01
  Scenario: User accepts cookies
    Then User clicks on accept cookies

  #TC02
  Scenario: User navigates to biometrics section
    When User is on the home page
    And User clicks on Our Service Menu
    And User clicks on health science
    And User clicks on clinical research
    Then User navigates to Biometrics page

  #TC03
  Scenario: User downloads the pdf
    When User is on the Biometrics page
    Then Download the Biometrics Services PDF

  #TC04
  Scenario: User takes screenshot of biometrics page
    When User is on the Biometrics page
    Then User takes screenshot of biometrics page
