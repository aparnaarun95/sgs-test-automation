package StepDefinitions;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
		features="src/test/resources/Features",
		glue= {"StepDefinitions"},
		monochrome=true,
		plugin={"pretty",
				"json:target/JSONReports/report.json",
				"html:target/HTMLReports/htmlReport.html",
		"junit:target/JUNITReports/report.xml"}

		)
public class TestRunner extends AbstractTestNGCucumberTests {
}
