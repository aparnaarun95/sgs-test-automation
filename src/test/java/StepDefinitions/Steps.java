package StepDefinitions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.AcceptCookies;
import pages.Biometrics;

public class Steps {

	ChromeOptions options = getChromeOptions();
	public WebDriver driver;
	AcceptCookies accept;
	Biometrics biometrics;

	@After
	public void close(Scenario scenario) throws IOException {
		if (scenario.isFailed()) {
			//take screenshot
			final byte[] screenshot = ((TakesScreenshot)this.driver).getScreenshotAs(OutputType.BYTES);
			scenario.attach(screenshot, "image/png", "" );
			try (FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir") + File.separator + "Error-Screenshots" + File.separator + "Screenshot_" +scenario.getName()+ "screenshot.png")) {
				fos.write(screenshot);
			}

		}
		driver.quit();
	}


	private ChromeOptions getChromeOptions() 
	{
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();  
		chromePrefs.put("plugins.always_open_pdf_externally", true);
		chromePrefs.put("download.default_directory",System.getProperty("user.dir") +  File.separator + "PDFDownloads");   

		ChromeOptions options = new ChromeOptions();  
		options.setExperimentalOption("prefs", chromePrefs);   

		return options;
	}

	@Given("User navigates to SGS portal")
	public void user_navigates_to_sgs_portal() {
		ChromeOptions chromeCapabilities = new ChromeOptions();
		chromeCapabilities.addArguments("start-maximized");
		driver = new ChromeDriver(getChromeOptions());


		driver.get("https://www.sgs.com/en-be");
		driver.manage().window().maximize();
	}
	@Then("User clicks on accept cookies")
	public void user_clicks_on_accept_cookies() throws IOException {
		AcceptCookies accept= new AcceptCookies(driver);
		accept.clickOnAccept();
	}

	//TC02

	@When("User is on the home page")
	public void user_is_on_home_page() throws IOException {
		user_clicks_on_accept_cookies();
	}
	@When("User clicks on Our Service Menu")
	public void user_clicks_on_our_service_menu() {
		Biometrics biometrics= new Biometrics(driver);
		biometrics.clickOnOurServicesButton();
	}
	@When("User clicks on health science")
	public void user_clicks_on_health_science_button() {
		Biometrics biometrics= new Biometrics(driver);
		biometrics.clickOnHealthScienceButton();
	}
	@When("User clicks on clinical research")
	public void user_clicks_on_clinical_research_button() throws InterruptedException {
		Biometrics biometrics= new Biometrics(driver);
		biometrics.clickOnClinicalResearchButton();
	}
	@Then("User navigates to Biometrics page")
	public void user_navigates_to_biometrics_section() throws InterruptedException, IOException {
		Biometrics biometrics= new Biometrics(driver);
		biometrics.clickOnBiometricsButton();
	}

	//TC03

	@When("User is on the Biometrics page")
	public void user_is_on_biometrics_page() throws InterruptedException, IOException  {
		user_is_on_home_page();
		user_clicks_on_our_service_menu();
		user_clicks_on_health_science_button();
		user_clicks_on_clinical_research_button();
		user_navigates_to_biometrics_section();

	}
	@Then("Download the Biometrics Services PDF")
	public void user_download_biometrics_pdf() throws InterruptedException, FileNotFoundException, IOException {
		Biometrics biometrics= new Biometrics(driver);
		biometrics.clickOnBiometricsServicesPDFButton();
	}

	//TC04
	@Then("User takes screenshot of biometrics page")
	public void user_takes_screenshot_of_biometrics_page() throws InterruptedException, IOException {
		Biometrics biometrics= new Biometrics(driver);
		biometrics.biometricsScreenshot();
	}
}
