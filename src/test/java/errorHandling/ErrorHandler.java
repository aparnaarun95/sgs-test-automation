package errorHandling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class ErrorHandler {

	public static void handleError(WebDriver driver) throws FileNotFoundException, IOException {
		final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		try (FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir") + File.separator + "Error-Screenshots" + File.separator + "Screenshot_" +ste[1].getMethodName()+ "screenshot.png")) {
			fos.write(screenshot);
		}
	}
}
