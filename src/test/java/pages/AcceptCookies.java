package pages;

import java.io.IOException;
import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import errorHandling.ErrorHandler;


public class AcceptCookies {

	WebDriver driver;
	private By acceptCookieButton=By.xpath("//button[@id='onetrust-accept-btn-handler']");
	private By rejectCookieButton=By.xpath("//button[@id='onetrust-reject-all-handler']']");

	public AcceptCookies(WebDriver driver) {
		this.driver =driver;
	}

	public void clickOnAccept() throws IOException {
		//To wait for element visible
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOfElementLocated(acceptCookieButton));
		try {
			driver.findElement(acceptCookieButton).click();
		}
		catch (StaleElementReferenceException |ElementNotInteractableException e) {
			ErrorHandler.handleError(driver);
		}
	}
	

	public void clickOnReject() {
		driver.findElement(rejectCookieButton).click();
	}
}