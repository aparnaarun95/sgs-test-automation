package pages;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import errorHandling.ErrorHandler;

public class Biometrics {

	public WebDriver driver;
	private By ourServicesButton=By.xpath("//span[text()='Our Services']");
	private By healthScienceButton=By.xpath("//a[text()='Health Science']");
	private By clinicalResearchButton=By.xpath("//h3[text()='Clinical Research']");
	private By biometricsButton=By.xpath("//h3[text()='Biometrics']");
	private By biometricsServicesPDFButton=By.xpath("//span[text()='Biometrics Services']");
	private By biometricsHeaderDescriptionHeader=By.xpath("//div[contains(text(),'Comprehensive biometric outsourcing solutions')]");
	
	public Biometrics(WebDriver driver) {
		this.driver =driver;
	}

	public void clickOnOurServicesButton() {
		driver.findElement(ourServicesButton).click();
	}
	public void clickOnHealthScienceButton() {
		driver.findElement(healthScienceButton).click();
	}


	public void clickOnClinicalResearchButton() throws InterruptedException  {
		//scroll down the window
		JavascriptExecutor js = (JavascriptExecutor) driver;	
		js.executeScript("window.scroll(0,1500)");
		
		//To wait for element visible
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOfElementLocated(clinicalResearchButton));
		
		driver.findElement(clinicalResearchButton).click();
	}
	public void clickOnBiometricsButton() throws InterruptedException, FileNotFoundException, IOException  {
		
		try {
			//scroll down the window
			JavascriptExecutor js = (JavascriptExecutor) driver;	
			js.executeScript("window.scroll(0,1500)");
			
			//To wait for element visible
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
			wait.until(ExpectedConditions.visibilityOfElementLocated(biometricsButton));
			
			driver.findElement(biometricsButton).click();
			//To wait for element visible
			wait.until(ExpectedConditions.visibilityOfElementLocated(biometricsHeaderDescriptionHeader));
		} catch (Exception ex) {
			ErrorHandler.handleError(driver);
		}

	}

	public void clickOnBiometricsServicesPDFButton() throws InterruptedException, FileNotFoundException, IOException {
		try {
			//scroll down the window
			JavascriptExecutor js = (JavascriptExecutor) driver;	
			js.executeScript("window.scroll(0,1500)");
	
			//To wait for element visible
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
			wait.until(ExpectedConditions.visibilityOfElementLocated(biometricsServicesPDFButton));
					
			driver.findElement(biometricsServicesPDFButton).click();
			Thread.sleep(5000);	
		} catch (Exception ex) {
			ErrorHandler.handleError(driver);
		}		


	}
	public void biometricsScreenshot() throws IOException, InterruptedException {
		try {
			//To wait for element visible
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
			wait.until(ExpectedConditions.visibilityOfElementLocated(biometricsHeaderDescriptionHeader));
							
			File screenshotFile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshotFile, new File(System.getProperty("user.dir") +  File.separator + "Biometrics-Screenshot" + File.separator + "screenshot.png"));
		} catch (Exception ex) {
			ErrorHandler.handleError(driver);
		}	

	}

}