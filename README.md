SGS Test Automation 
********************

* This project is used to store the test automation project for the SGS website.
* The project uses Selenium with TestNG to create and run the test cases.
* The project is written using Java.

- The development tickets for test cases implemented are located at `SGS-Test-Automation/Development-tickets.txt `
- The downloaded pdf document will be available at `SGS-Test-Automation/PDFDownloads/`
- The biometric page screenshot will be available at `SGS-Test-Automation/Biometrics-Screenshot/`
- The error screenshots will be available at `SGS-Test-Automation/Error-Screenshots/`


The execution proof is located at [(https://www.loom.com/share/1edcd193e0444c009c0cf340c4205aad)]

Remarks: For the test scenarios 3 and 4, it is also possible to go to the page directly using the url in browser, but since it was not specified in the reuirements, I have made it sequential. 

